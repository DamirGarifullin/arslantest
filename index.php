<?php

spl_autoload_register(function ($class) {
    include 'Clinic/'.str_replace('_', '/', $class).'.php';
});

define ("MAX_PATIENTS", 10);

echo "\n";
echo "Clinic v0.1\n";
echo "To iterate press Enter.\n";
echo "To quit type 'quit', 'q', 'exit' or 'abort' then press Enter.\n";
echo "\n";
echo "Patients appear every iteration.\n";
echo "Emulation goes like this: Therapist (TER) -> Specialists (LOR or OKU) -> Therapist -> Exit. \n";
echo "Therapist's reception duration is 2 iterations.\n";
echo "Specialists' reception duration is 3 iterations.\n";
echo "\n";

$clinic = DataGenerator::getClinic();
$patients = new SplQueue();

while (true) {
    
    $currentPatient = DataGenerator::generateRandomPatientData($clinic->aComplaints);
    $currentPatient->sendToSpecialists($clinic->aSpecialists[0]);
    $patients->enqueue($currentPatient);
    
    foreach ($clinic->aSpecialists as $s) {
        $s->tick();
    }
    foreach ($patients as $p) {
        $p->tick();
    }
    
    for ($i=count($patients)-1; $i>=0; $i--) {
        echo $patients[$i] . "\n";
    }
    echo "-------------------------------------------------------\n";
    for ($i=0; $i<count($clinic->aSpecialists); $i++) {
        echo $clinic->aSpecialists[$i] . "\n";
    }
    echo "=======================================================\n";

    $handle = fopen ("php://stdin","r");
    $line = fgets($handle);
    if(in_array(trim($line), ['quit', 'q', 'exit', 'abort'])){
        echo "ABORTING!\n";
        exit;
    }
}