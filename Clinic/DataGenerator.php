<?php

class DataGenerator {
    
    static function getClinic() {
        $aComplaints = DataGenerator::getComplaints();
        $aSpecialities = DataGenerator::getSpecialities($aComplaints);
        $aSpecialists = DataGenerator::getSpecialists($aSpecialities);
        $clinic = new Clinic_Common($aComplaints, $aSpecialities, $aSpecialists);
        $aSpecialists[0]->clinic = $clinic;
        return $clinic;
    }
    
    static function getComplaints() {
        return array (
            new Complaint("Poor eyesight"),
            new Complaint("Cataract"),
            new Complaint("Glaucoma"),
            new Complaint("Throat pain"),
            new Complaint("Ear pain")
        );
    }
    
    static function getSpecialities($complaints) {
        return array (
            new Speciality("TER", array()),
            new Speciality("LOR", array_slice($complaints, 3, 2)),
            new Speciality("OCU", array_slice($complaints, 0, 3))
        );
    }

    static function getSpecialists($specialities) {
        return array (
            new Specialist_Therapist("TER1", $specialities[0], 2),
            new Specialist_Common("LOR1", $specialities[1], 3),
            new Specialist_Common("OCU1", $specialities[2], 3)
        );
    }

    static function generateRandomPatientData($allPossibleComplaints) {        
        static $id = 0;
        $id = ($id+1) % 100;

        $name = self::getRandomName();

        $complaints = array();
        $complainsCount = rand(1, 2);
        $currentComplaintsIndices = array_rand($allPossibleComplaints, $complainsCount);
        if ($complainsCount == 1) {
            $currentComplaintsIndices = array($currentComplaintsIndices);
        }
        foreach ($currentComplaintsIndices as $ci) {
            array_push($complaints, $allPossibleComplaints[$ci]);
        }
        return new Patient($id, $name, $complaints);    
    }

    static function getRandomName() {
        $upperCaseLetters = str_split("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        $lowerCaseLetters = str_split("abcdefghijklmnopqrstuvwxyz");
        $name = "";
        for ($i=0; $i<2; $i++) {
            for ($j=0; $j<rand(2, 5); $j++) {
                if ($j==0) {
                    $name .= $upperCaseLetters[array_rand($upperCaseLetters)];
                } else {
                    $name .= $lowerCaseLetters[array_rand($lowerCaseLetters)];
                }
            }
            if ($i==0) {
                $name .= ' ';
            }
        }
        return $name;
    }
}