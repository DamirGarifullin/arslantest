<?php

class Speciality {
    public $name;
    public $aComplaints;
    
    public function __construct($name, $aComplaints) {
        $this->name = $name;
        $this->aComplaints = $aComplaints;
    }
}