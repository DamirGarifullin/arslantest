<?php

class Patient implements Tick {
    
    public $id;
    public $name;
    public $aComplaints;
    public $aReferrals; // направления
    protected $_currentReferral;
    public $hasLeft = false;
    
    public function __construct($id, $name, $aComplaints) {
        $this->id = $id;
        $this->name = $name;
        $this->aComplaints = $aComplaints;
        $this->aReferrals = array();
    }
    
    public function __toString() {
        $s = sprintf('%02d', $this->id) . ' ' . $this->name . ":\t";
        $complaints = array_map(function($c){return $c->name;}, $this->aComplaints);
        $s .= implode(", ", $complaints);
        if (!empty($this->aReferrals)) {
            $referrals = array_map(function($r){return $r->specialist->name;}, $this->aReferrals);
            $s .= " -> " . implode(", ", $referrals);
        }
        if ($this->hasLeft) {
            $s .= " ... has left.";
        }
        return $s;
    }
        
    public function sendToSpecialists($specialists) {
        
        if (!is_array($specialists)) {
            $this->sendToSpecialists(array($specialists));
        } else {
            foreach ($specialists as $s) {
                $r = array_filter($this->aReferrals, function ($r) use ($s) {
                    return $r->specialist == $s;
                });
                if (empty($r)) {
                    array_push($this->aReferrals, new Referral($s));
                }
            }
        }        
    }
    
    public function endVisit() {
        $this->_currentReferral->hasVisited = true;
    }
    
    public function isSecondaryVisit() {
        return !is_null($this->_currentReferral) && $this->_currentReferral->bSecondaryVisit;
    }
    
    // Tick
    
    public function tick() {
        $vals = array_values($this->aReferrals);
        if (!empty($vals)) {
            foreach ($vals as $r) {
                if (!$r->hasVisited && $r==$this->_currentReferral) {
                    return;
                }
            }
            foreach ($vals as $r) {
                if (!$r->hasVisited && $r!=$this->_currentReferral) {
                    $r->specialist->visit($this);
                    $this->_currentReferral = $r;
                    return;
                }
            }
        }
        // Вторичный визит к терапевту
        $therapistReferral = $this->aReferrals[0];
        $therapistReferral->bSecondaryVisit = true;
        $therapistReferral->hasVisited = false;
        $this->tick();
    }
}
