<?php

class Referral {
    public $specialist;
    public $hasVisited = false;
    public $bSecondaryVisit = false;
    
    public function __construct($specialist) {
        $this->specialist = $specialist;
    }
}