<?php

class Clinic_Common implements Clinic_Interface {
    
    public $aComplaints;
    public $aSpecialities;
    public $aSpecialists;
 
    public function __construct($aComplaints, $aSpecialities, $aSpecialists) {
        $this->aComplaints = $aComplaints;
        $this->aSpecialities = $aSpecialities;
        $this->aSpecialists = $aSpecialists;
    }
    
    // Clinic_Interface
    
    public function getAllSpecialities() {
        return $this->aSpecialities;
    }
    
    public function getSpecialists($speciality) {
        $a = array_filter($this->aSpecialists, function ($s) use ($speciality) {
            return $s->speciality == $speciality;
        });        
        return $a;
    }
}