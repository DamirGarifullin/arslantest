<?php

interface Clinic_Interface {
    public function getAllSpecialities();
    public function getSpecialists($speciality);
}