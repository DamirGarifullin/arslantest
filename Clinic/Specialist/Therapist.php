<?php

class Specialist_Therapist extends Specialist_Base {
    
    public $clinic;

    public function makeDecision() {
        
        if ($this->_currentPatient->isSecondaryVisit()) {
            $this->_currentPatient->hasLeft = true;
            return;
        }
        
        $specialities = $this->clinic->getAllSpecialities();
        $determinedSpeciality = $this->findSpecialitiesForComplaints($specialities, $this->_currentPatient->aComplaints);
        $determinedSpecialists = array();
        foreach ($determinedSpeciality as $ds) {
            $specialistsOfOneSpeciality = $this->clinic->getSpecialists($ds);
            array_push($determinedSpecialists, array_values($specialistsOfOneSpeciality)[0]);
        }
        $this->_currentPatient->sendToSpecialists($determinedSpecialists);
    }
    
    private function findSpecialitiesForComplaints($specialities, $complaints) {
        $result = array();
        foreach ($specialities as $s) {
            foreach ($complaints as $c) {
                if (in_array($c, $s->aComplaints) && !in_array($s, $result)) {
                    array_push($result, $s);
                }
            }
        }
        return $result;
    }
}