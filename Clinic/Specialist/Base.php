<?php

abstract class Specialist_Base implements Tick {
   
    public $name;
    public $speciality;
    public $receptionDuration;
    public $patients;
    
    public function __construct($name, $speciality, $receptionDuration) {
        $this->name = $name;
        $this->speciality = $speciality;
        $this->receptionDuration = $receptionDuration;
        $this->patients = new SplQueue();
    }
    
    public function __toString() {
        $s = $this->name . ': ';
        if (isset($this->_currentPatient)) {
            $s .= sprintf('%02d', $this->_currentPatient->id) . '|';
        } else {
            $s .= '  |';
        }
        foreach ($this->patients as $p) {
            $s .= sprintf('%02d', $p->id) . ' ';
        }
        return $s;
    }
    
    public function visit($patient) {
        $this->patients->enqueue($patient);
    }
    
    // Tick
    
    protected $_currentPatient;
    protected $_ticks = 0;
    
    public function tick() {
        if (!isset($this->_currentPatient)) {
            if (count($this->patients) > 0) {
                $this->_currentPatient = $this->patients->dequeue();
                $this->_ticks = 0;
            }
        }
        else {
            $this->_ticks++;
            if ($this->_ticks >= $this->receptionDuration) {
                $this->makeDecision();
                $this->_currentPatient->endVisit();
                unset($this->_currentPatient);
                $this->tick();
            }
        }
    }
    
    public abstract function makeDecision();
}